import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { TestComponent } from './test/test.component';
import { HomeComponent } from './core/components/home/home.component';
import { UpdateProfilComponent } from './features/authentication/components/update-profil/update-profil.component';
import { LearningSpaceComponent } from './features/user-space/components/learning-space/learning-space.component';
import { InvoiceComponent } from './features/billing/components/invoice/invoice.component';

const routes: Routes = [

  {
    path: '', component: HomeComponent
  },

  {
    path: 'test', component: TestComponent
  },

  {
    path: 'connexion',
    loadChildren: () => import('./features/authentication/authentication.module').then(m => m.AuthenticationModule)
  },
  {
    path: 'panier',
    loadChildren: () => import('./features/cart/cart.module').then(m => m.CartModule)
  },
  {
    path: 'apprentissage', loadChildren: () => import('./features/user-space/user-space.module').then(m => m.UserSpaceModule)
  },
  {
    path: 'historique', 
    loadChildren: () => import('./features/billing/billing.module').then(m => m.BillingModule),
   
  },


];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }