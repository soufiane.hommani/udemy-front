import { User } from "../core/classes/user";
import { ArticleState } from "../core/core-store/types/articlesState";
import { UserState } from "../features/authentication/auth-store/userState";
import { InvoiceState } from "../features/billing/billing-store/invoice-state";
import { CartState } from "../features/cart/store/cartState";
import { CourseState } from "../features/user-space/store/types/courseState";


export interface AppStateInterface {
  
  articles: ArticleState;
  courses: CourseState;
  user: UserState;
  cart: CartState;
  invoices: InvoiceState;

}
