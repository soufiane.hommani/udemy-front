import { Component } from '@angular/core';
import { AppStateInterface } from './app-store/appState.interface';
import { Store } from '@ngrx/store';
import * as UserActions from './features/authentication/auth-store/actions';
import * as CoursesActions from './features/user-space/store/actions';
import * as CartActions from './features/cart/store/actions';
import { Article } from './core/classes/article';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'udemy';

  constructor( private store: Store<AppStateInterface>){

    try {
      
      //console.log('localStorage.getItem(user)',localStorage.getItem('user') )
      /* if(localStorage.getItem('user') == 'user'){
        return;
      } */
      const user = JSON.parse(localStorage.getItem('user'));
      if (user) {
        delete user.orders;
        const courses = JSON.parse(localStorage.getItem('courses'));

        if(localStorage.getItem('cart')?.length! !== 0){

          const cart = this.removeDoublons(JSON.parse(localStorage.getItem('cart')));
          this.store.dispatch(CartActions.restoreCart({articles: cart}));
        }

        this.store.dispatch(UserActions.loadUserSuccess({ user: user }));
        this.store.dispatch(CoursesActions.loadCoursesSuccess({courses: courses}));
      }
    } catch (error) {
      console.error(error);
    }

  }

  removeDoublons(cart: Article[]) {
    let uniqueCart = [];
    let cartIds = [];

    cart?.forEach((article) => {
      if (!cartIds.includes(article.id)) {
        uniqueCart.push(article);
        cartIds.push(article.id);
      }
    });

    return uniqueCart;
  }


  




}
