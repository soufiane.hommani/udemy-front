import { LOCALE_ID, NgModule, isDevMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from "./core/core.module";
import { TestComponent } from './test/test.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpClientModule } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { Store, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { first } from 'rxjs';
import { userReducer } from './features/authentication/auth-store/reducers';
import { coursesReducer } from './features/user-space/store/reducers';
import { cartReducer } from './features/cart/store/reducers';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTabsModule} from '@angular/material/tabs';
import { invoiceReducer } from './features/billing/billing-store/reducers';
import {MatIconModule} from '@angular/material/icon';

// the second parameter 'fr' is optional
registerLocaleData(localeFr, 'fr');


@NgModule({
    declarations: [
        AppComponent,
        TestComponent,
        TestComponent,

    ],
    providers: [

        { provide: LOCALE_ID, useValue: 'fr' }
        
    ],
    bootstrap: [AppComponent],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppRoutingModule,
        MatTabsModule,
        CoreModule,
        FormsModule,
        ReactiveFormsModule,
        FontAwesomeModule,
        MatIconModule,
        StoreModule.forRoot({user: userReducer, courses: coursesReducer, cart: cartReducer, 
          invoices: invoiceReducer}),
        StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: !isDevMode() }),
        EffectsModule.forRoot(),
        

    ]
})
export class AppModule {

  /*   public constructor(private store: Store<unknown>) {
        this.exposeStoreToGlobal();
      }

    public exposeStoreToGlobal() {
          globalThis.showStoreSnapshot = () => {
            this.store.pipe(first()).subscribe((snapshot) => console.log(snapshot));
          };
      } */
 }
