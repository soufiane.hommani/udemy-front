import { Category } from "./category";

export class Article {


    id: number;
    name: string;
    description: string;
    category: Category;
    thumbnailLocation: string;
    price: number;

}
