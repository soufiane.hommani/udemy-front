import { User } from "./user";

export interface Invoice {
    id: number;
    date: Date;
    amount: number;
    customer: User;
    order: any;
  }
  