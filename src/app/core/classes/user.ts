export class User {

    id: number;
    firstname: string;
    lastname: string;
    username: string;
    adress: string;
    password: string;

    constructor(firstname: string, lastname: string, username: string,
         adress: string, password: string) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.adress = adress;
        this.password = password;
    }


}
