import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { CartService } from '../../../features/cart/services/cart.service';
import { Article } from '../../classes/article';
import { AppStateInterface } from 'src/app/app-store/appState.interface';
import { Store, select } from '@ngrx/store';
import { Observable, async } from 'rxjs';
import { User } from '../../classes/user';
import { getUser } from 'src/app/features/authentication/auth-store/selectors';
import * as CartActions from '../../../features/cart/store/actions';

import { Router } from '@angular/router';

@Component({
  selector: 'article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  @Input() article: Article;

  truncatedArticle: Article;
  canAddToCart: boolean;
  idSubStr: string;

  user$: Observable<User>;


  constructor(private cartService: CartService, private store: Store<AppStateInterface>,
  ) {

    this.user$ = this.store.pipe(select(getUser));

  }


  ngOnInit() {

    if (this.article) {
      this.truncatedArticle = { ...this.article };
      if (this.truncatedArticle.description) {
        this.truncatedArticle.description = this.truncatedArticle.description.replace(/^(.{60}[^\s]*).*/, "$1") + "...";
      }
      if (this.truncatedArticle.id) {
        this.idSubStr = `"id":${this.truncatedArticle.id}`;
        this.canAddToCart = (localStorage.getItem('cart') !== null && localStorage.getItem('cart').includes(this.idSubStr)) ? false : true;
      }
    }

  }

  addToCart(article: Article) {
    this; this.cartService.addToCart(article);
  }
  isAlreadyInCart(article: Article): boolean {
    return this.cartService.isAlreadyInCart(article);
  }
  


}
