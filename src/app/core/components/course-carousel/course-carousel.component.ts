import { Component, Input } from '@angular/core';
import { Article } from '../../classes/article';

@Component({
  selector: 'course-carousel',
  templateUrl: './course-carousel.component.html',
  styleUrls: ['./course-carousel.component.scss']
})
export class CourseCarouselComponent {

  @Input() courses: Article[] = [];

  activeIndex = 0;

  showNextCourse() {

    this.activeIndex = (this.activeIndex + 1) % this.courses.length;
    console.log('this.activeIndex', this.activeIndex)
  }

  showPreviousCourse(){

    this.activeIndex = (this.activeIndex - 1) % this.courses.length;

  }



}
