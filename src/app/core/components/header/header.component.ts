import { Component, OnInit } from '@angular/core';
import { faCartShopping, faFilm, faMagnifyingGlass, faUser } from '@fortawesome/free-solid-svg-icons';
import { AuthenticationService } from 'src/app/features/authentication/services/authentication.service';
import { User } from '../../classes/user';
import { CartService } from '../../../features/cart/services/cart.service';
import { Article } from '../../classes/article';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AppStateInterface } from 'src/app/app-store/appState.interface';
import { getLoading, getUser } from 'src/app/features/authentication/auth-store/selectors';
import * as UserActions from '../../../features/authentication/auth-store/actions';
import { coursesSelector } from 'src/app/features/user-space/store/selectors';
import * as CoursesActions from '../../../features/user-space/store/actions';
import { selectArticles, selectTotal } from 'src/app/features/cart/store/selectors';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  userIcon = faUser;
  cartIcon = faCartShopping;
  magnifyingGlassIcon = faMagnifyingGlass;
  user$: Observable<User>;
  courses$: Observable<Article[]>;
  cartArticles: Article[] = [];
  loading$: Observable<boolean>;

  total$: Observable<number>;
  cartArticles$: Observable<Article[]>


  constructor(private auth: AuthenticationService,
    private router: Router, private store: Store<AppStateInterface>) {


    this.user$ = this.store.pipe(select(getUser));
    this.loading$ = this.store.pipe(select(getLoading));
    this.courses$ = this.store.pipe(select(coursesSelector));

    this.total$ = this.store.select(selectTotal);
    this.cartArticles$ = this.store.select(selectArticles);


  }

  ngOnInit() {


  
  }

  logout() {
    // this.store.dispatch(UserAction.logoutUser());
    this.auth.logout();
  }

  routeToCart() {
    this.router.navigate(['panier']);
  }

  preventReloadingPage($event) {
    $event.preventDefault();
  }

}

