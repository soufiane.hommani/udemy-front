import { Component, OnDestroy } from '@angular/core';
import { Article } from '../../classes/article';
import { ArticlesService } from 'src/app/core/services/articles.service';
import { AuthenticationService } from 'src/app/features/authentication/services/authentication.service';
import { User } from '../../classes/user';
import { AppStateInterface } from '../../../app-store/appState.interface';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription, filter, of } from 'rxjs';
import { getArticles, getError, getLoading } from '../../core-store/selectors';
import * as ArticlesAction from '../../core-store/actions';
import { getUser } from 'src/app/features/authentication/auth-store/selectors';
import { coursesSelector } from 'src/app/features/user-space/store/selectors';
import * as CoursesActions from '../../../features/user-space/store/actions';

import * as _ from 'lodash';


@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnDestroy {


  HomeSubscriptions: Subscription[] = [];

  isLoading$: Observable<boolean>;
  error$: Observable<string | null>;
  articles$: Observable<Article[]>;
  courses$: Observable<Article[]>;
  nonPurchasedArticles$: Observable<Article[]>;
  user$: Observable<User>;

  constructor(private auth: AuthenticationService,
     private store: Store<AppStateInterface>) {

    this.isLoading$ = this.store.pipe(select(getLoading));
    this.error$ = this.store.pipe(select(getError));
    this.articles$ = this.store.pipe(select(getArticles));
    this.user$ = this.store.pipe(select(getUser));
    this.courses$ = this.store.pipe(select(coursesSelector));


  }
  ngOnDestroy(): void {
    // this.HomeSubscriptions.map(s => s.unsubscribe());
  }


  courses: Article[] = [];
  async ngOnInit() {
    this.store.dispatch(ArticlesAction.loadArticles())

    this.courses$.subscribe(c => {

     this.courses = c;
     console.log('this.courses', this.courses)
    })

    this.user$.subscribe(us => {
      if (!us) { return }
      this.store.dispatch(CoursesActions.loadCourses({ userId: us?.id }));
    })

    this.courses$.subscribe(courses => {
      this.articles$.subscribe(articles => {

        localStorage.setItem('courses', JSON.stringify(courses));
        this.nonPurchasedArticles$ = of(_.differenceWith(articles, courses, (a, b) => a.id === b.id));
        
      });
    })

  }

  cutSentence(courseDescription: String): String{
    return courseDescription.length > 30 ? courseDescription.substring(0, 30) + '...' : courseDescription;
  }

  activeIndex = 0;

  showNextCourse() {

    this.activeIndex = (this.activeIndex + 1) % this.courses.length;
    console.log('this.activeIndex', this.activeIndex)
  }

  showPreviousCourse(){

    this.activeIndex = (this.activeIndex - 1) % this.courses.length;

  }


}
