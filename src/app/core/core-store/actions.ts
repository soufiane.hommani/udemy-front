import { createAction, props } from '@ngrx/store';
import { Article } from '../classes/article';


export const loadArticles = createAction('[Articles] Load Articles');
export const loadArticlesSuccess = createAction(
  '[Articles] Load Articles Success',
  props<{ articles: Article[] }>()
);
export const loadArticlesFailure = createAction(
  '[Articles] Load Articles Failure',
  props<{ error: any }>()
);

export const addArticle = createAction(
  '[Articles] Add Article',
  props<{ article: Article }>()
);
export const addArticleSuccess = createAction(
  '[Articles] Add Article Success',
  props<{ article: Article }>()
);
export const addArticleFailure = createAction(
  '[Articles] Add Article Failure',
  props<{ error: any }>()
);

export const removeArticle = createAction(
  '[Articles] Remove Article',
  props<{ articleId: number }>()
);
export const removeArticleSuccess = createAction(
  '[Articles] Remove Article Success',
  props<{ articleId: number }>()
);
export const removeArticleFailure = createAction(
  '[Articles] Remove Article Failure',
  props<{ error: any }>()
);
