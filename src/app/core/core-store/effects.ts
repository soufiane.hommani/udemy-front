import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { switchMap, map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ArticlesService } from '../services/articles.service';
import * as ArticleActions from './actions';

@Injectable()
export class ArticleEffects {

    constructor(private actions$: Actions, private articlesService: ArticlesService) { }

    loadArticles$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ArticleActions.loadArticles),
            switchMap(() => this.articlesService.fetchArticles().pipe(
                map(articles => ArticleActions.loadArticlesSuccess({ articles })),
                catchError(error => of(ArticleActions.loadArticlesFailure({ error })))
            ))
        )
    );

    /*   addArticle$ = createEffect(() =>
        this.actions$.pipe(
          ofType(ArticleActions.addArticle),
          switchMap(({ article }) => this.articlesService.addArticle(article).pipe(
            map(article => ArticleActions.addArticleSuccess({ article })),
            catchError(error => of(ArticleActions.addArticleFailure({ error })))
          ))
        )
      );
    
      removeArticle$ = createEffect(() =>
        this.actions$.pipe(
          ofType(ArticleActions.removeArticle),
          switchMap(({ articleId }) => this.articlesService.removeArticle(articleId).pipe(
            map(() => ArticleActions.removeArticleSuccess({ articleId })),
            catchError(error => of(ArticleActions.removeArticleFailure({ error })))
          ))
        )
      );
     */
}
