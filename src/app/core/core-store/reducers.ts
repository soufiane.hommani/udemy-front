import { ArticleState } from "./types/articlesState";
import * as ArticleActions from './actions';
import { createReducer, on } from "@ngrx/store";


export const initialState: ArticleState = {
    items: [],
    loading: false,
    error: null
};

export const articleReducers = createReducer(
    initialState,
    on(ArticleActions.loadArticles, state => {
        return {
            ...state,
            loading: true,
            error: null
        }
    }),
    on(ArticleActions.loadArticlesSuccess, (state, { articles }) => {
        console.log('articles with category Id', articles)
        return {
            ...state,
            loading: false,
            items: articles
        }
    }),
    on(ArticleActions.loadArticlesFailure, (state, { error }) => {
        return {
            ...state,
            loading: false,
            error
        }
    }),
    on(ArticleActions.addArticle, (state, { article }) => {
        return {
            ...state,
            items: [...state.items, article]
        }
    }),
    on(ArticleActions.addArticleSuccess, (state, { article }) => {
        return {
            ...state,
            items: [...state.items, article]
        }
    }),
    on(ArticleActions.addArticleFailure, (state, { error }) => {
        return {
            ...state,
            error
        }
    }),
    on(ArticleActions.removeArticle, (state, { articleId }) => {
        return {
            ...state,
            items: state.items.filter(article => article.id !== articleId)
        }
    }),
    on(ArticleActions.removeArticleSuccess, (state, { articleId }) => {
        return {
            ...state,
            items: state.items.filter(article => article.id !== articleId)
        }
    }),
    on(ArticleActions.removeArticleFailure, (state, { error }) => {
        return {
            ...state,
            error
        }
    }),
);