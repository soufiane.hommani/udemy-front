import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ArticleState } from './types/articlesState';


const getArticleState = createFeatureSelector<ArticleState>('articles');

export const getArticles = createSelector(
  getArticleState,
  (state: ArticleState) => state.items
);

export const getLoading = createSelector(
  getArticleState,
  (state: ArticleState) => state.loading
);

export const getError = createSelector(
  getArticleState,
  (state: ArticleState) => state.error
);
