import { Article } from "../../classes/article";

export interface ArticleState {
    items: Article[];
    loading: boolean;
    error: any;
}
