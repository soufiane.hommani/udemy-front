import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HomeComponent } from './components/home/home.component';
import { User } from './classes/user';
import { ArticleComponent } from './components/article/article.component';
import { FooterComponent } from './components/footer/footer.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { articleReducers } from './core-store/reducers';
import { ArticleEffects } from './core-store/effects';
import {MatCardModule} from '@angular/material/card';
import { CourseCarouselComponent } from './components/course-carousel/course-carousel.component';




@NgModule({
  declarations: [
    HeaderComponent,
    HomeComponent,
    ArticleComponent,
    FooterComponent,
    CourseCarouselComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    StoreModule.forFeature('articles', articleReducers),
    EffectsModule.forFeature([ArticleEffects]),
    FontAwesomeModule,
    MatCardModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    ArticleComponent
  ]
})
export class CoreModule { }
