import { Injectable } from '@angular/core';
import { Article } from '../classes/article';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthenticationService } from '../../features/authentication/services/authentication.service';
import { ArticleState } from '../core-store/types/articlesState';
import { CourseState } from 'src/app/features/user-space/store/types/courseState';

@Injectable({
  providedIn: 'root'
})


//Articles in catalogue
export class ArticlesService {

  constructor(private http: HttpClient) {
  }

  fetchArticles(alreadyBoughtArticles?: Article[]): Observable<Article[]> {

    return this.http.get<Article[]>('http://localhost:8080/udemy/article',
      { headers: new HttpHeaders({ "Content-Type": "application/json" }) });

  }

}
