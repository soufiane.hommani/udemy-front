import { createAction, props } from '@ngrx/store';
import { User } from '../../../core/classes/user';

export const loadUser = createAction(
  '[User] Load User'
);

export const loadUserSuccess = createAction(
  '[User] Load User Success',
  props<{ user: User }>()
);

export const loadUserError = createAction(
  '[User] Load User Error',
  props<{ error: string }>()
);

export const loginUser = createAction(
  '[User] Login User',
  props<{ username: string, password: string }>()
);

export const loginUserSuccess = createAction(
  '[User] Login User Success',
  props<{ user: User }>()
);

export const loginUserError = createAction(
  '[User] Login User Error',
  props<{ error: string }>()
);

export const logoutUser = createAction(
  '[User] Logout User'
);
export const logoutUserError = createAction(
    '[User] Logout User Error',
    props<{ error: string }>()
  );
  

  export const updateUser = createAction(
    '[User] Update User',
    props<{ user: User }>()
  );
  
  export const updateUserSuccess = createAction(
    '[User] Update User Success',
    props<{ user: User }>()
  );
  
  export const updateUserError = createAction(
    '[User] Update User Error',
    props<{ error: string }>()
  );


  export const createUser = createAction(
    '[User] Create User',
    props<{ user: User }>()
  );

  export const createUserSuccess = createAction(
    '[User] Create User Success',
    props<{ user: User }>()
  );

  export const createUserFailure = createAction(
    '[User] Create User Failure',
    props<{ error: String }>()
    );
  