import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/features/authentication/services/authentication.service';
import * as UserActions from './actions';
import { User } from '../../../core/classes/user';

import { HttpClient } from '@angular/common/http';
import { UserService } from '../services/user.service';
import { Store } from '@ngrx/store';
import { AppStateInterface } from 'src/app/app-store/appState.interface';


@Injectable()
export class UserEffects {

    constructor(private store: Store<AppStateInterface>, private http: HttpClient, private authService: AuthenticationService,
       private actions$: Actions, private userService: UserService){}

  loadUser$ = createEffect(() => this.actions$.pipe(
    ofType(UserActions.loadUser),
    switchMap(() => this.authService.fetchUserInBDD(14)
      .pipe(
        map((user: User) => UserActions.loadUserSuccess({ user })),
        catchError(error => of(UserActions.loadUserError({ error: error.message })))
      )
    )
  )); 

  loginUser$ = createEffect(() => this.actions$.pipe(
    ofType(UserActions.loginUser),
    mergeMap(({ username, password }) => this.authService.authenticate(username, password)
      .pipe(
        tap((user: User) => {
          if (user === null) {
            this.store.dispatch(UserActions.loginUserError({ error: 'Invalid credentials' }));
          }
        }),
        map((user: User) => {


            localStorage.setItem('user', JSON.stringify(user))
            // extract the id from the user object and pass it as an argument to the fetchUserInBDD method
            return UserActions.loginUserSuccess({ user });
        }),
        catchError(error => of(UserActions.loginUserError({ error: 'Invalid credentials' })))
      )
    )
  ));


  logoutUser$ = createEffect(() => this.actions$.pipe(
    ofType(UserActions.logoutUser),
    switchMap(() => {
        sessionStorage.removeItem('user');
       
        return of(UserActions.logoutUser());
    }),
    catchError(error => of(UserActions.logoutUserError({ error: error.message })))
  ));


  updateUser$ = createEffect(() => this.actions$.pipe(
    ofType(UserActions.updateUser),
    switchMap(({ user }) => {
        return this.userService.updateUserInfos(user)
            .pipe(
                map((updatedUser: User) => {
                    localStorage.setItem('user', JSON.stringify(updatedUser));
                    return UserActions.updateUserSuccess({ user: updatedUser });
                }),
                catchError(error => of(UserActions.updateUserError({ error: error.message })))
            )
    })
));


createUser$ = createEffect(() => this.actions$.pipe(
  ofType(UserActions.createUser),
  switchMap(({ user }) => {
      return this.authService.registerUser(user)
          .pipe(
          /*   tap((user: User) => {
              if (user === null) {

                console.log('passe par leoeeoe', )
                this.store.dispatch(UserActions.createUserFailure({ error: 'already created' }));
              }
            }), */
              map((created: User) => {
                
                
                
                if(created){
                  localStorage.setItem('user', JSON.stringify(created));
                  return UserActions.createUserSuccess({user: created});
                } else {
                  return UserActions.createUserFailure({error: 'already created' });
                }
              }),
              catchError(error => of(UserActions.createUserFailure({error: 'already created' })))
          )
  })
));

}