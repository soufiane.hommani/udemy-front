import { createReducer, on } from '@ngrx/store';
import { User } from '../../../core/classes/user';
import * as UserActions from './actions';
import { UserState } from './userState';

const initialState: UserState = {
    user: null,
    isLoading: false,
    error: null
};

export const userReducer = createReducer(
    initialState,
    on(UserActions.loadUser, state => {
        return { ...state, isLoading: true, error: null };
    }),
    on(UserActions.loadUserSuccess, (state, { user }) => {
        return { ...state, user, isLoading: false };
    }),
    on(UserActions.loadUserError, (state, { error }) => {
        return { ...state, error, isLoading: false };
    }),
    on(UserActions.loginUser, state => {
        return { ...state, isLoading: true, error: null };
    }),
    on(UserActions.loginUserSuccess, (state, { user }) => {
        return { ...state, user, isLoading: false };
    }),
    on(UserActions.loginUserError, (state, { error }) => {
        return { ...state, error, isLoading: false };
    }),
    on(UserActions.logoutUser, state => {
        return { ...state, user: null };
    }),
    on(UserActions.updateUser, (state, { user }) => {
        return { ...state, user };
    }),
    on(UserActions.createUser, (state, { user }) => {
        return { ...state, user };
    }),
    on(UserActions.createUserSuccess, (state, { user }) => {
        return { ...state, user };
    }),
    on(UserActions.createUserFailure, (state, { error }) => {
        return { ...state, error: error as string, isLoading: false };
    }),
    
    
   /*  on(UserActions.logoutUser, state => {
        return { ...state, courses: [] };
        }), */
);

