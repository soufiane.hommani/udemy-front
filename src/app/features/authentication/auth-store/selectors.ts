import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UserState } from '../auth-store/userState';


const getUserState = createFeatureSelector<UserState>('user');

export const getUser = createSelector(
  getUserState,
  (state: UserState) => state?.user
);

export const getLoading = createSelector(
  getUserState,
  (state: UserState) => state?.isLoading
);

export const getError = createSelector(
  getUserState,
  (state: UserState) => state.error
);

