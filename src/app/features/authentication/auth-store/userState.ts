import { User } from '../../../core/classes/user';

export interface UserState {
  user: User | null;
  isLoading: boolean;
  error: string | null;
}
