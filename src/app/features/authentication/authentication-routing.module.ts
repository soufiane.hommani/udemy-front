import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistrationFormComponent } from './components/registration-form/registration-form.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { CartComponent } from '../cart/components/cart/cart.component';
import { LearningSpaceComponent } from '../user-space/components/learning-space/learning-space.component';
import { UpdateProfilComponent } from './components/update-profil/update-profil.component';

const routes: Routes = [
  {
    path: '', component: LoginFormComponent
  },
  {
    path: 'panier', component: CartComponent
  },
  {
    path: 'inscription', component: RegistrationFormComponent
  },
  {
    path:'update-profil', component: UpdateProfilComponent
  },
  
 

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule { }
