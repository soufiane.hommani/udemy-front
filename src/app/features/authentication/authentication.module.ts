import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthenticationRoutingModule } from './authentication-routing.module';
import { RegistrationFormComponent } from './components/registration-form/registration-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthenticationService } from './services/authentication.service';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { CartComponent } from '../cart/components/cart/cart.component';
import { LearningSpaceComponent } from '../user-space/components/learning-space/learning-space.component';
import { StoreModule } from '@ngrx/store';
import { userReducer } from './auth-store/reducers';
import { UserEffects } from './auth-store/effects';
import { EffectsModule } from '@ngrx/effects';
import { UpdateProfilComponent } from './components/update-profil/update-profil.component';


@NgModule({
  declarations: [
    RegistrationFormComponent,
    LoginFormComponent,
    LearningSpaceComponent,
    UpdateProfilComponent
  ],
  imports: [
    CommonModule,
    AuthenticationRoutingModule,
    ReactiveFormsModule,
    StoreModule.forFeature('user', userReducer),
    EffectsModule.forFeature([UserEffects]),
  ],
  providers: [],
  exports: [
    RegistrationFormComponent,
    LoginFormComponent,
    UpdateProfilComponent
  ],

})
export class AuthenticationModule { }
