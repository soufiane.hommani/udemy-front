import { ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { Observable, Subscription, map } from 'rxjs';
import * as UserActions from '../../auth-store/actions';
import { Store, select } from '@ngrx/store';
import { AppStateInterface } from 'src/app/app-store/appState.interface';
import { User } from 'src/app/core/classes/user';
import * as CoursesActions from '../../../user-space/store/actions';
import { getError, getUser } from '../../auth-store/selectors';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnDestroy {


  loginForm: FormGroup;
  isSubmitted: boolean = false;
  message: string = "";

  user$: Observable<User>;

  LoginFormSubscriptions: Subscription[] = [];

  constructor(private fb: FormBuilder, private readonly changeDetectorRef: ChangeDetectorRef,
    private auth: AuthenticationService, private router: Router, private store: Store<AppStateInterface>) {



    this.user$ = this.store.pipe(select(getUser));

    this.loginForm = fb.group({
      username: ['', Validators.required],
      password: [null, [Validators.required, Validators.min(2)]],
      rememberMe: false
    });

  }
  ngOnDestroy(): void {
    //this.LoginFormSubscriptions.map(s => s.unsubscribe());
  }


  ngOnInit(): void {

    this.loginForm.valueChanges.pipe(
      map(() => {
        this.isSubmitted = false
        this.message = '';
      })
    ).subscribe();

    
    
    this.user$.subscribe(u => {

      
      this.store.select(getError).subscribe(error => {
        console.log('error', error)
        if(this.isSubmitted){
          this.message = error;
        }
      });
      
      //only successful createdUser get an id from the back-end
      if (u?.id) {
        this.router.navigate(['']);
      }
    })


  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }


  async onSubmitLogin() {

    this.isSubmitted = true;
    
    this.message = '';

    if (this.loginForm.invalid) {
      this.isSubmitted = true;
      console.log('invalid')
      return;
    }


    const { username, password } = this.loginForm.value;


    await this.store.dispatch(UserActions.loginUser({ username: username, password: password }));



    /* let error = this.store.select(getError);
    console.log('error', error)
    this.message = error === null ? 'failure' : '';
    console.log('this.message', this.message) */

    /* this.auth.authenticate(username, password).subscribe(
      (user: User) => {


        localStorage.setItem('user', JSON.stringify(user));
        localStorage.setItem('cart', '');

        this.router.navigate(['']);


      },
      error => {
        this.store.dispatch(UserActions.loginUserError({ error }));
        this.isSubmitted = true;
      }
    );
 */




  }

}
