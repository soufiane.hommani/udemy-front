import { AfterViewChecked, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, map, tap } from 'rxjs';
import { User } from 'src/app/core/classes/user';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';
import { AppStateInterface } from 'src/app/app-store/appState.interface';
import { Store, select } from '@ngrx/store';
import * as UserActions from '../../../authentication/auth-store/actions';
import { getError, getUser } from '../../auth-store/selectors';


@Component({
  selector: 'registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss']
})
export class RegistrationFormComponent implements OnInit, AfterViewChecked {



  registrationForm: FormGroup;
  isSubmitted: boolean = false;
  userToRegister: User;
  message: string = "";
  user$: Observable<User>;


  constructor(private fb: FormBuilder, private readonly changeDetectorRef: ChangeDetectorRef,
    private auth: AuthenticationService,
    private router: Router, private store: Store<AppStateInterface>) {

    this.user$ = this.store.pipe(select(getUser));


    this.registrationForm = fb.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      username: ['', Validators.required],
      adress: '',
      password: [null, [Validators.required, Validators.min(2)]],
      agreement: false
    });

  }



  ngOnInit(): void {

    this.registrationForm.valueChanges.pipe(
      map(() => {
        this.isSubmitted = false
        this.message = '';
      })

    ).subscribe();

    this.store.select(getError).subscribe((error) => {
      console.log('error here', error)
      this.message = error;
      if (this.isSubmitted) {
      }
    });


    this.user$.subscribe(u => {






      //only successful createdUser get an id from the back-end
      if (u?.id) {
        this.router.navigate(['']);
      }
    })

  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  async onSubmit() {

    this.message = '';
    console.log('this.message on submit', this.message)

    if (this.registrationForm.invalid) {
      this.isSubmitted = true;
      console.log('invalid')
      return;
    }

    const { firstname, lastname, username, adress, password } = this.registrationForm.value;

    //console.log(firstname, lastname, username, adress, password)

    this.userToRegister = new User(firstname, lastname, username, adress, password);

    if (this.userToRegister)
      await this.store.dispatch(UserActions.createUser({ user: this.userToRegister }));

    /*   this.store.select(getError).subscribe(error => {
        this.message = error === null ? 'already created' : '';
        console.log('this.message', this.message)
        this.store.dispatch(UserActions.createUserSuccess({user: null}));
      }); */

  }

}
