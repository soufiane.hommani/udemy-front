import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AfterViewChecked, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { User } from '../../../../core/classes/user';
import { Observable, map, tap } from 'rxjs';
import { AuthenticationService } from '../../services/authentication.service';
import { Store, select } from '@ngrx/store';
import { AppStateInterface } from 'src/app/app-store/appState.interface';
import { getUser } from '../../auth-store/selectors';
import { UserService } from '../../services/user.service';
import * as UserActions from '../../auth-store/actions';



@Component({
  selector: 'app-update-profil',
  templateUrl: './update-profil.component.html',
  styleUrls: ['./update-profil.component.scss']
})
export class UpdateProfilComponent implements OnInit, AfterViewChecked {


  updateUserForm: FormGroup;
  isSubmitted: boolean = false;
  user$: Observable<User>;
  updatedUser: User;
  canModify: boolean = false;

  id: number;
  p: any;
  message: string;


  constructor(private auth: AuthenticationService, private http: HttpClient, private route: ActivatedRoute,
    private fb: FormBuilder, private readonly changeDetectorRef: ChangeDetectorRef, private router: Router,
    private store: Store<AppStateInterface>, private userService: UserService,) {



      

    this.updateUserForm = fb.group({
      firstname: [Validators.required],
      lastname: [Validators.required],
      username: [Validators.required],
      adress: '',
    });

    this.user$ = this.store.pipe(select(getUser));

    this.user$.subscribe((u: User) => {

      if(!u){
        
        console.log('u ttt ', u)
        this.message = "already created";
        return;
      }

      this.updatedUser = u;

      if(this.updateUserForm){
        // Your code here
        this.updateUserForm.setValue({
          firstname: u.firstname,
          lastname: u.lastname,
          username: u.username,
          adress: u.adress,
        });
        }

    })

  }

  async ngOnInit() {

    this.updateUserForm.valueChanges.pipe(
      map(() => this.isSubmitted = false)

    ).subscribe(() => {
      this.canModify = this.canModifyUser();
    })

  }

  canModifyUser(): boolean {
    let isFormModified = false;
    for (const key in this.updateUserForm.controls) {
      if (this.updateUserForm.controls.hasOwnProperty(key)) {
        const control = this.updateUserForm.controls[key];
        if (control.dirty || control.touched) {
          isFormModified = true;
          break;
        }

      }
    }
    return isFormModified;
  }

  update() {

    if (!this.canModify) {
      return;
    }

    this.updatedUser = {...this.updatedUser, ...this.updateUserForm.value}
    this.store.dispatch(UserActions.updateUser({ user: this.updatedUser }))

  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }



}
