import { Injectable } from '@angular/core';
import { AuthenticationModule } from '../authentication.module';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from 'src/app/core/classes/user';
import { BehaviorSubject, Observable } from 'rxjs';
import { CartService } from '../../cart/services/cart.service';
import { ArticlesService } from 'src/app/core/services/articles.service';
import * as UserActions from '../../../features/authentication/auth-store/actions';
import { AppStateInterface } from 'src/app/app-store/appState.interface';
import { Store } from '@ngrx/store';
import * as CoursesActions from '../../user-space/store/actions';
import * as CartActions from '../../cart/store/actions';
import { UserService } from './user.service';





@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {



  updateUser(body) {

    return this.http.put("http://localhost:8080/udemy/user", body, {
      headers: new HttpHeaders({ "Content-Type": "application/json" }),
      responseType: 'text'
    });

  }


  fetchUserInBDD(id: number) {
    return this.http.get<User>(`http://localhost:8080/udemy/user/${id}`);
  }

  constructor(private http: HttpClient, 
    private articlesService: ArticlesService,
     private store: Store<AppStateInterface>) {


  }

  registerUser(user: User) {

    const body = user;

    return this.http.post<User>("http://localhost:8080/udemy/user", body, {
      headers: new HttpHeaders({ "Content-Type": "application/json" })
    });

  }

  authenticate(username: string, password: string): Observable<User> {

    return this.http.post<User>(" http://localhost:8080/udemy/user/signin", [username, password], {
      headers: new HttpHeaders({ "Content-Type": "application/json" }),
    });
  }

  logout() {
    localStorage.removeItem('user');
    localStorage.removeItem('cart');
    //window.location.reload();  
    this.store.dispatch(UserActions.loadUserSuccess({ user: null }));
    this.store.dispatch(CartActions.clearCart());
    this.store.dispatch(CoursesActions.resetCoursesState());

   // return this.http.post('http://localhost:8080/udemy/user/logout', {});
  }

}

