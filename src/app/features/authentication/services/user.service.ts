import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/classes/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }


  updateUserInfos(updatedUser: User): Observable<User> {
    const body = updatedUser;
    return this.http.put<User>(`http://localhost:8080/udemy/user`, body
    );
  }



}
