import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BillingContainerComponent } from './components/billing-container/billing-container.component';
import { InvoiceComponent } from './components/invoice/invoice.component';



const routes: Routes = [
 
  {
    path: '', component: BillingContainerComponent
  },
  {
    path: 'facture/:orderId', component: InvoiceComponent
  }

];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes),
    CommonModule
  ],
  exports: [RouterModule]
})
export class BillingRoutingModule { }
