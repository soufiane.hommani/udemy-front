import { createAction, props } from "@ngrx/store";
import { Invoice } from "src/app/core/classes/invoice";

export const loadInvoices = createAction(
    '[Invoices] Load Invoices'
);

export const loadInvoicesSuccess = createAction(
    '[Invoices] Load Invoices Success',
    props<{ invoices: Invoice[] }>()
);

export const loadInvoicesFail = createAction(
    '[Invoices] Load Invoices Fail',
    props<{ error: any }>()
);