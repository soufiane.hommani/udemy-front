import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { InvoiceService } from '../services/invoice.service';
import * as invoiceActions from './actions';

@Injectable()
export class InvoiceEffects {

  loadInvoices$ = createEffect(() =>
    this.actions$.pipe(
      ofType(invoiceActions.loadInvoices),
      mergeMap(() =>
        this.invoiceService.fetchInvoice().pipe(
          map(invoices => invoiceActions.loadInvoicesSuccess({ invoices })),
          catchError(error => of(invoiceActions.loadInvoicesFail({ error })))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private invoiceService: InvoiceService
  ) {}
}

