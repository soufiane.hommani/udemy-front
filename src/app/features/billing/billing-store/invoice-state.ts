import { Invoice } from "src/app/core/classes/invoice";

export interface InvoiceState {
    invoices: Invoice[];
    loading: boolean;
    error?: string;
  }