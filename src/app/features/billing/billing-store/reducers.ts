import { createReducer, on, Action } from '@ngrx/store';
import * as invoiceActions from './actions';
import { Invoice } from 'src/app/core/classes/invoice';

export interface InvoiceState {
  invoices: Invoice[];
  loading: boolean;
  error: any;
}

const initialState: InvoiceState = {
  invoices: [],
  loading: false,
  error: null
};

export const invoiceReducer = createReducer(
  initialState,
  on(invoiceActions.loadInvoices, state => ({
    ...state,
    loading: true,
    error: null
  })),
  on(invoiceActions.loadInvoicesSuccess, (state, { invoices }) => ({
    ...state,
    invoices,
    loading: false
  })),
  on(invoiceActions.loadInvoicesFail, (state, { error }) => ({
    ...state,
    loading: false,
    error
  }))
);

