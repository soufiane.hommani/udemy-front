import { createSelector, createFeatureSelector } from "@ngrx/store";
import { InvoiceState } from "./invoice-state";

const getInvoiceState = createFeatureSelector<InvoiceState>("invoices");

export const getInvoices = createSelector(
getInvoiceState,
state => state.invoices
);


export const selectInvoicesError = createSelector(
getInvoiceState,
state => state.error
);