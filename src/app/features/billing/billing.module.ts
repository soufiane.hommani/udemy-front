import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BillingRoutingModule } from './billing-routing.module';
import { BillingContainerComponent } from './components/billing-container/billing-container.component';
import {MatTabsModule} from '@angular/material/tabs';
import { CoursesListComponent } from './components/courses-list/courses-list.component';
import {MatTableModule} from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { InvoiceEffects } from './billing-store/effects';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { invoiceReducer } from '../billing/billing-store/reducers';
import {MatIconModule} from '@angular/material/icon';
import { InvoiceComponent } from './components/invoice/invoice.component';




@NgModule({
  declarations: [
    BillingContainerComponent,
    CoursesListComponent,
    InvoiceComponent
  ],
  imports: [
    CommonModule,
    BillingRoutingModule,
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    StoreModule.forFeature('invoices', invoiceReducer),
    EffectsModule.forFeature([InvoiceEffects]),
    MatIconModule
    
  ]
})
export class BillingModule { }
