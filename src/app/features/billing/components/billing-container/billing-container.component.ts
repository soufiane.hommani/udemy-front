import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AppStateInterface } from 'src/app/app-store/appState.interface';
import * as invoicesAction from '../../billing-store/actions';
import { Observable } from 'rxjs';
import { Invoice } from 'src/app/core/classes/invoice';
import { getLoading } from 'src/app/core/core-store/selectors';
import { getInvoices } from '../../billing-store/selectors';



@Component({
  selector: 'app-billing-container',
  templateUrl: './billing-container.component.html',
  styleUrls: ['./billing-container.component.scss']
})
export class BillingContainerComponent implements OnInit {

  invoices$: Observable<Invoice[]>;

  constructor(private store: Store<AppStateInterface>) {

    this.invoices$ = this.store.pipe(select(getInvoices));

  }

  ngOnInit() {
    this.store.dispatch(invoicesAction.loadInvoices())

   

  }

}
