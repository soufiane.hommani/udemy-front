import { Component, ViewChild, AfterViewInit, Input, SimpleChanges, Inject } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import { Invoice } from 'src/app/core/classes/invoice';
import { Router } from '@angular/router';

@Component({
  selector: 'courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.scss']
})
export class CoursesListComponent implements AfterViewInit{

  @Input() invoices: Invoice[] = [];

  displayedColumns: string[] = [ 'firstName', 'totalPrice','date', "payment-method"];
  dataSource = new MatTableDataSource<Invoice>(this.invoices);

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private route: Router){

  }

  routeToReceipt(invoice: Invoice) {
    console.log('invoice', invoice)
    this.route.navigate(['historique/facture', invoice.id]); 
   }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['invoices'] && changes['invoices'].currentValue) {
      this.dataSource = new MatTableDataSource(changes['invoices'].currentValue);
      this.dataSource.paginator = this.paginator;
    }
  }


  cutSentence(courseDescription: String): String{
    return courseDescription.length > 30 ? courseDescription.substring(0, 30) + '...' : courseDescription;
  }


}




const ELEMENT_DATA: Invoice[] = [];
