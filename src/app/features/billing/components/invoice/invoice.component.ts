import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable, map } from 'rxjs';
import { AppStateInterface } from 'src/app/app-store/appState.interface';
import { Invoice } from 'src/app/core/classes/invoice';
import { getInvoices } from '../../billing-store/selectors';
import * as invoicesAction from '../../billing-store/actions';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {

  orderId: number;

  orderdInvoice: any;
  invoices$: Observable<Invoice[]>;

  constructor(private route: ActivatedRoute,
    private store: Store<AppStateInterface>) {

    this.invoices$ = this.store.pipe(select(getInvoices));
  }

  ngOnInit() {

    this.store.dispatch(invoicesAction.loadInvoices())


    this.route.params.subscribe(params => {
      this.orderId = params['orderId'];

      this.invoices$.subscribe((invoices) => {
        this.orderId
        
        this.orderdInvoice = invoices.find(invoice => invoice.id == this.orderId);
      });


    });



  }

}
