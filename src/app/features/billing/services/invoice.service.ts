import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, async, flatMap, map } from 'rxjs';
import { AppStateInterface } from 'src/app/app-store/appState.interface';
import { Invoice } from 'src/app/core/classes/invoice';
import { User } from 'src/app/core/classes/user';
import { getUser } from '../../authentication/auth-store/selectors';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  user$: Observable<User>;

  constructor(private http: HttpClient,
    private store: Store<AppStateInterface>) { 
    this.user$ = this.store.pipe(select(getUser));
  }

  fetchInvoice(): Observable<Invoice[]> {
    return this.user$.pipe(
      flatMap(user => this.http.get<Invoice[]>(`http://localhost:8080/udemy/invoice/${user.id}`, {
        headers: new HttpHeaders({ "Content-Type": "application/json" })
      }))
    );
  }

}


