import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Article } from 'src/app/core/classes/article';
import { CartService } from '../../services/cart.service';
import { selectArticles, selectTotal } from '../../store/selectors';
import { AppStateInterface } from 'src/app/app-store/appState.interface';
import { User } from 'src/app/core/classes/user';
import { getUser } from 'src/app/features/authentication/auth-store/selectors';
import * as cartActions from '../../store/actions';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {


  total: number;
  cartArticles: Article[] = [];
  coursesArticle: Article[] = [];
  justBoughtArticles:  Article[] = [];

  total$: Observable<number>;
  cartArticles$: Observable<Article[]>;
  user$: Observable<User>;
  user: User;

  constructor(private cart: CartService, 
     private store: Store<AppStateInterface>) {

    this.total$ = this.store.select(selectTotal);
    this.cartArticles$ = this.store.select(selectArticles);
    this.user$ = this.store.pipe(select(getUser));

  }

  ngOnInit() {
    this.cartArticles$.subscribe((c: Article[]) => {
      this.cartArticles = c;
    })

    this.user$.subscribe((u: User) => {
      this.user = u;
    })
  
  }

  removeArticle(article: Article) {
    this.cart.removeArticle(article);
  }

  validateOrder() {
    this.justBoughtArticles = [...this.cartArticles]
    this.store.dispatch(cartActions.sendOrder({user: this.user, cart: this.cartArticles}));
    
  }

}
