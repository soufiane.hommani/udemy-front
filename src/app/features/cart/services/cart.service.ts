import { Injectable } from '@angular/core';
import { Article } from 'src/app/core/classes/article';
import * as CartActions from '../../../features/cart/store/actions';
import { Store, select } from '@ngrx/store';
import { AppStateInterface } from 'src/app/app-store/appState.interface';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { User } from 'src/app/core/classes/user';
import { getUser } from '../../authentication/auth-store/selectors';
import { Order } from 'src/app/core/classes/order';


@Injectable({
  providedIn: 'root'
})
export class CartService {





  constructor(private router: Router,
    private store: Store<AppStateInterface>,
    private http: HttpClient) {




  }




  addToCart(article: Article) {
    if (!(localStorage.getItem('user'))) {
      this.router.navigate(['connexion']);
    } else {

      let cart = [];

      if (localStorage.getItem('cart')) {
        cart = JSON.parse(localStorage.getItem('cart'))
      }
      if (!this.isAlreadyInCart(article)) {
        cart.push(article);
        localStorage.setItem('cart', JSON.stringify(cart))
        this.store.dispatch(CartActions.addToCart({ article: article }))
      }
    }

  }

  removeArticle(article: Article) {

    this.removeArticleFromLocalStorage(article.id);
    this.store.dispatch(CartActions.removeFromCart({ articleId: article.id }))

  }


  removeArticleFromLocalStorage(id: number) {
    let cart = JSON.parse(localStorage.getItem('cart'));
    cart = cart.filter(article => article.id !== id);
    localStorage.setItem('cart', JSON.stringify(cart));
  }





  validateOrder(u: User, cart: Article[]): Observable<Order> {

    if (!u || !cart) { return of(null) };

    const order = {
      "userId": u.id,
      "articlesId": cart.map((art: Article) => art.id)
    }


    return this.http.post<Order>("http://localhost:8080/udemy/orders", order, {
      headers: new HttpHeaders({ "Content-Type": "application/json" }),
      responseType: 'json'
    });



  }

  isAlreadyInCart(article: Article): boolean {
    let isInCart = false;

    this.store.select(state => state.cart.articles)
      .subscribe(articles => {
        articles.forEach(item => {
          if (item.id === article.id) {
            isInCart = true;
          }
        });
      });
    return isInCart;
  }




}
