import { createAction, props } from '@ngrx/store';
import { Article } from 'src/app/core/classes/article';
import { Order } from 'src/app/core/classes/order';
import { User } from 'src/app/core/classes/user';
import * as articleActions from '../../../core/core-store/actions';


export const addToCart = createAction(
  '[Cart] Add Item',
  props<{ article: Article }>()
);

export const removeFromCart = createAction(
  '[Cart] Remove Item',
  props<{ articleId: number }>()
);

export const clearCart = createAction(
  '[Cart] Clear'
);

export const restoreCart = createAction(
  '[Cart] Restore',
  props<{ articles: Article[] }>()
);


export const sendOrder = createAction(
  '[Cart] Send Order',
  props<{ user: User, cart: Article[] }>()
);

export const sendOrderSuccess = createAction(
  '[Cart] Send Order Success'
);


export const sendOrderFailure = createAction(
  '[Cart] Send Order Failure',
  props<{ error: any }>()
);


