import { Article } from 'src/app/core/classes/article';

export interface CartState {
  articles: Article[];
  total: number;
}

