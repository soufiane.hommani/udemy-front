import { Actions, createEffect, ofType } from "@ngrx/effects";
import { CartService } from "../services/cart.service";
import { AppStateInterface } from "src/app/app-store/appState.interface";
import { Store } from "@ngrx/store";
import { Injectable } from "@angular/core";
import { catchError, map, of, switchMap, tap } from "rxjs";
import { clearCart, sendOrder, sendOrderFailure, sendOrderSuccess } from "./actions";
import * as ArticleActions from '../../../core/core-store/actions';
import * as CoursesActions from '../../../features/user-space/store/actions';

@Injectable()
export class CartEffects {
    constructor(private actions$: Actions,
        private cartService: CartService, private store: Store<AppStateInterface>) { }

    sendOrder$ = createEffect(() =>
        this.actions$.pipe(
            ofType(sendOrder),
            switchMap(({ user, cart }) => {
                return this.cartService.validateOrder(user, cart).pipe(
                    map(() => sendOrderSuccess()),
                    tap(() => {
                        //this.store.dispatch(ArticleActions.loadArticles());
                        this.store.dispatch(CoursesActions.loadCourses({ userId: user?.id }));
                        this.store.dispatch(clearCart());
                        localStorage.removeItem('cart');
                    }),
                    catchError(error => of(
                        sendOrderFailure({ error }),
                    ))
                );
            })
        )
    );
}
