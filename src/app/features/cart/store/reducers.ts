import { createReducer, on } from '@ngrx/store';
import { addToCart, removeFromCart, clearCart, restoreCart, sendOrderSuccess } from './actions';
import { CartState } from './cartState';
import { Article } from 'src/app/core/classes/article';


export const initialCartState: CartState = {
  articles: [],
  total: 0
};


export const cartReducer = createReducer(
  initialCartState,
  on(addToCart, (state, { article }) => {
    return {
      ...state,
      articles: [...state.articles, article],
      total: state.total + article.price
    };
  }),
  on(removeFromCart, (state, { articleId }) => {
    const articleIndex = state.articles.findIndex(i => i.id === articleId);
    if (articleIndex === -1) {
      return state;
    }
    const removedArticle = state.articles[articleIndex];
    return {
      ...state,
      articles: [...state.articles.slice(0, articleIndex), ...state.articles.slice(articleIndex + 1)],
      total: state.total - removedArticle.price
    };
  }),
  on(clearCart, state => {
    return {
      ...state,
      articles: [],
      total: 0
    };
  }),
  on(restoreCart, (state, { articles }) => {
    return {
      ...state,
      articles: [...articles],
      total: articles.reduce((acc, cur) => acc + cur.price, 0)
    };
  }),
  on(sendOrderSuccess, state => {
    return {
      ...state,
      articles: [],
      total: 0
    };
  })

);
