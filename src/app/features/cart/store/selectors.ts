import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CartState } from './cartState';

export const selectCartState = createFeatureSelector<CartState>('cart');

export const selectArticles = createSelector(
  selectCartState,
  (state: CartState) => state.articles
);

export const selectTotal = createSelector(
  selectCartState,
  (state: CartState) => state.total
);
