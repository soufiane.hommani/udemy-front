import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppStateInterface } from 'src/app/app-store/appState.interface';
import { Article } from 'src/app/core/classes/article';
import { CoursesService } from 'src/app/features/user-space/services/courses.service';
import { coursesSelector } from '../../store/selectors';
import * as CoursesActions from '../../store/actions';
import { getUser } from 'src/app/features/authentication/auth-store/selectors';
import { User } from 'src/app/core/classes/user';


@Component({
  selector: 'app-learning-space',
  templateUrl: './learning-space.component.html',
  styleUrls: ['./learning-space.component.scss']
})
export class LearningSpaceComponent {


  courses$: Observable<Article[]>;
  user$: Observable<User>;

  constructor(private store: Store<AppStateInterface>) {

    this.courses$ = this.store.pipe(select(coursesSelector));
    this.user$ = this.store.pipe(select(getUser));

  }

  ngOnInit(): void {
    this.user$.subscribe((u: User) => {
      console.log('u', u)
      if(!u){return}
      this.store.dispatch(CoursesActions.loadCourses({ userId: u.id }));
    })
  }

}






