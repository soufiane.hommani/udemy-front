import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Article } from 'src/app/core/classes/article';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from 'src/app/core/classes/user';


@Injectable({
  providedIn: 'root'
})
//Already Bought articles
export class CoursesService {
  
  constructor(private http: HttpClient) { }
  fetchCourses(userId: number): Observable<Article[]> {
    return this.http.get<Article[]>(`http://localhost:8080/udemy/article/byuser/${userId}`);
  }

}
