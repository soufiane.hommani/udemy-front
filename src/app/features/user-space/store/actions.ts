import { createAction, props } from "@ngrx/store";
import { Article } from "src/app/core/classes/article";

export const loadCourses = createAction('[courses] Load Courses', props<{userId: number}>());
export const loadCoursesSuccess = createAction(
  '[Courses] Load Courses Success',
  props<{ courses: Article[] }>()
);
export const loadCoursesFailure = createAction(
  '[Courses] Load Courses Failure',
  props<{ error: any }>()
);
export const resetCoursesState = createAction(
  '[Courses] Reset Courses State'
  );