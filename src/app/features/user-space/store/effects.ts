import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, of, switchMap } from 'rxjs';
import { CoursesService } from '../services/courses.service';
import * as CourseActions from "src/app/features/user-space/store/actions";
import { User } from 'src/app/core/classes/user';

@Injectable()
export class CoursesEffects {
    constructor(private actions$: Actions, private coursesService: CoursesService) { }

    loadCourses$ = createEffect(() =>
        this.actions$.pipe(
            ofType<any>(CourseActions.loadCourses),
            switchMap(({userId}) => {
                if (!userId) {
                    return of(CourseActions.loadCoursesFailure({ error: 'userId is undefined' }))
                }
                return this.coursesService.fetchCourses(userId).pipe(
                    map(courses => CourseActions.loadCoursesSuccess({ courses })),
                    catchError(error => of(CourseActions.loadCoursesFailure({ error })))
                )
            })
        )
    );
}
