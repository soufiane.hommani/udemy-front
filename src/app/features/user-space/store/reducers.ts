import { CourseState } from "src/app/features/user-space/store/types/courseState";
import * as CourseActions from "src/app/features/user-space/store/actions";
import { createReducer, on } from "@ngrx/store";
export const initialState: CourseState = {
    courses: [],
    loading: false,
    error: null
  };

  export const coursesReducer = createReducer(
    initialState,
    on(CourseActions.loadCourses, state => {
      return {
        ...state,
        loading: true,
        error: null
      };
    }),
    on(CourseActions.loadCoursesSuccess, (state, { courses }) => {
      return {
        ...state,
        courses: courses,
        loading: false
      };
    }),
    on(CourseActions.loadCoursesFailure, (state, { error }) => {
      return {
        ...state,
        error,
        loading: false
      };
    }),
    on(CourseActions.resetCoursesState, state => {
      return initialState;
      }),
  );