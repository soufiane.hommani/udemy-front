import { createFeatureSelector, createSelector } from "@ngrx/store";
import { CourseState } from "./types/courseState";
import { ArticleState } from "src/app/core/core-store/types/articlesState";

const getCoursesState = createFeatureSelector<CourseState>('courses');

export const coursesSelector = createSelector(
    getCoursesState,
    (state: CourseState) => state?.courses
);

export const getLoading = createSelector(
    getCoursesState,
    (state: CourseState) => state?.loading
);

export const getCoursesError = createSelector(
    getCoursesState,
    (state: CourseState) => state?.error
);

