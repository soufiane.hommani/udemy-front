import { Article } from "src/app/core/classes/article";

export interface CourseState {
    courses: Article[];
    loading: boolean;
    error: any;
}
