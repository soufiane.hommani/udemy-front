import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LearningSpaceComponent } from './components/learning-space/learning-space.component';
import { RouterModule } from '@angular/router';

const routes = [
  {
    path: '', component: LearningSpaceComponent

  }
]

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserSpaceRoutingModule { }
