import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { coursesReducer } from './store/reducers';
import { StoreModule } from '@ngrx/store';
import { CoursesEffects } from './store/effects';
import { EffectsModule } from '@ngrx/effects';
import { UserSpaceRoutingModule } from './user-space-routing.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature('courses', coursesReducer),
    EffectsModule.forFeature([CoursesEffects]),
    UserSpaceRoutingModule
  ]
})
export class UserSpaceModule { }
