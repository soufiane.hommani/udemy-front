import { Component } from '@angular/core';
import { faFilm, faUser } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})


export class TestComponent {

  filmIcon = faFilm;
  userIcon = faUser;


}
