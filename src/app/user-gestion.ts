export class UserGestion {

    id: number;
    adresse: string;
    firstname: string;
    lastname: string;
    password: string;
    username: string;
    version: number;
}
